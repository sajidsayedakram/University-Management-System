<?php
include_once ("../../vendor/autoload.php");
use App\Teacher\teacher;
$obj = new teacher();
$obj->setData($_GET);
$value = $obj->show();
?>
<?php
//if (!empty($_SESSION['user_info'])) {
//?>
<?php include_once"../header.php"; ?>


<?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <li><a style="color: #0a001f;" href="create.php">Add New Teacher</a></li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <!-- Bordered striped table -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h4 class="panel-title">Teacher Table
                        <?php
                            if (isset($_SESSION['message'])){
                                ?>
                        <button style="margin-left: 20px" class="btn btn-success" >
                                <?php
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                            } ?>
                        </button>
                    </h4>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>E-mail</th>
                            <th>Contact-No</th>
                            <th>Credit</th>
                            <th>Designation</th>
                            <th>Department</th>
                            <th>EDIT/DELETE</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($value as $item) {
                            ?>
                            <tr>
                                <td><?php echo $item['id']; ?></td>
                                <td><?php echo $item['title']; ?></td>
                                <td><?php echo $item['address']; ?></td>
                                <td><?php echo $item['mail']; ?></td>
                                <td><?php echo $item['contact']; ?></td>
                                <td><?php echo $item['credit']; ?></td>
                                <td><?php echo $item['designation_title']; ?></td>
                                <td><?php echo $item['dept_title']; ?></td>
                                <td>
                                    <a href="../Teacher/edit.php?id=<?php echo $item['id']; ?>" class="btn btn-success">Edit</a>
                                    <a href="../Teacher/delete.php?id=<?php echo $item['id']; ?>"  onclick="return confirm('Do You Want To Delete?')"  class="btn btn-danger">DELETE</a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /bordered striped table -->

        </div>
    </div>
    <!-- /main content -->

<?php
  include_once("../footer.php");
?>
<!--    --><?php
//	} else{
//		$_SESSION['fail']= "You are not authorized!";
//		header('location:../../../index.php');
//	}

?>