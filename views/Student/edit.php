<?php
include_once ("../../vendor/autoload.php");
use App\Student\student;
$obj = new student();
$obj->setData($_GET);
$value = $obj->view();

$value1 = $obj->DepartmentView();
?>
<?php
//if (!empty($_SESSION['user_info'])) {
//    ?>
    <?php include_once"../header.php"; ?>


    <?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span
                        class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
                <li><a style="color: #0a001f;" href="view.php">Student List</a></li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
    <div class="row">
    <div class="col-md-8">

        <!-- Basic layout-->
        <form action="update.php" class="form-horizontal" method="post">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h2 class="panel-title"> Update Your Data <span class="label label-success position-right" style="font-size: 14px"><?php
                            if (isset($_SESSION['update-message'])){
                                echo $_SESSION['update-message'];
                                unset($_SESSION['update-message']);
                            } ?></span></h2>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Name:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" value="<?php echo $value['title'];?>" placeholder="Student Name" name="title"required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Email:</label>
                        <div class="col-lg-9">
                            <input type="email" class="form-control"  value="<?php echo $value['mail'];?>" placeholder="Student Email" name="mail"required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Contact:</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control"  value="<?php echo $value['contact'];?>" placeholder="Student Contact Number" name="contact"required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Date:</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control"  value="<?php echo $value['date'];?>" name="date"required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Address:</label>
                        <div class="col-lg-9">
                            <textarea rows="7" cols="76" class="form-control" placeholder="Student Address" name="address"required><?php echo $value['address'];?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Department:</label>
                        <div class="col-lg-9">
                            <select data-placeholder="Select your state" name="department" class="select">
                                <optgroup label="Department">
                                    <option value="<?php echo $value['department'];?>"><?php echo $value['department'];?></option>
                                    <?php
                                    foreach ($value1 as $item) {
                                        if ($value['department'] != $item['title']) {
                                            ?>
                                            <option value="<?php echo $item['title']; ?>"><?php echo $item['title']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
                    </div>

                </div>
            </div>
            <input type="hidden" name="id" value="<?php echo $value['id']; ?>">
        </form>
        <!-- /basic layout -->
    </div>
    </div>
        <!-- /content area -->

    </div>
    </div>
    <!-- /main content -->

    <?php
    include_once("../footer.php");
    ?>

<!--    --><?php
//} else{
//    $_SESSION['fail']= "You are not authorized!";
//    header('location:../../../index.php');
//}

?>