<?php
include_once ("../../vendor/autoload.php");
use App\EnrollCourse\student;
$obj = new student();
$obj->setData($_GET);
$value = $obj->StudentView();
$value1 = $obj->CourseView();
//echo "<pre>";
//print_r($value);
//die();
?>
<?php include_once"../header.php"; ?>

<?php include_once("../Admin/side-menubar.php"); ?>

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>

                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <li><a style="color: #0a001f;" href="view.php">List Of Enroll Courses</a></li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-8">

                    <!-- Basic layout-->
                    <form action="store.php" class="form-horizontal" method="post">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h2 class="panel-title" >Enroll-In-A-Course
                                    <?php
                                        if (isset($_SESSION['message'])){
                                            ?>
                                    <button style="margin-left: 20px" class="btn btn-success" >
                                            <?php
                                            echo $_SESSION['message'];
                                            unset($_SESSION['message']);
                                        } ?>
                                    </button></h2>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Student Reg. No:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="student_id" id="student_id" class="form-control">
                                            <option value="">Select Student ID</option>
                                            <?php
                                            foreach ($value as $item) {
                                                ?>
                                                <option value="<?php echo $item['id'];?>"><?php echo $item['student_id'];?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Student Name:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="student_name" name="student_name" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Student Email:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="student_mail" name="student_mail" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Student Department:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="student_dept" name="student_dept" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Select Course:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="course" id="course" class="form-control">
                                            <optgroup label="Course">
                                                <option value="">Select Course</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Date:</label>
                                    <div class="col-lg-9">
                                        <input type="date" class="form-control" id="date" name="date">
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Assign<i class="icon-arrow-right14 position-right"></i></button>
                                </div>

                            </div>
                        </div>
                    </form>
                    <!-- /basic layout -->
                </div>

<?php
  include_once("../footer.php");
?>

<!--    		--><?php
//	} else{
//		$_SESSION['fail']= "You are not authorized!";
//		header('location:../../../index.php');
//	}

?>