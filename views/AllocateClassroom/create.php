<?php
include_once ("../../vendor/autoload.php");
use App\AllocateClassroom\classroom;
$obj = new classroom();
$obj->setData($_GET);
$value = $obj->DepartmentView();

$value1 = $obj->RoomView();

$value2 = $obj->DayView();
//echo "<pre>";
//print_r($value2);
//die();

?>
<?php include_once"../header.php"; ?>

<?php include_once("../Admin/side-menubar.php"); ?>

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>

                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <li><a style="color: #0a001f;" href="view.php">Class Schedule & Rooms</a></li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-8">

                    <!-- Basic layout-->
                    <form action="store.php" class="form-horizontal" method="post">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h2 class="panel-title" >Allocate Classrooms
                                        <?php
                                        if (isset($_SESSION['message'])){
                                         ?>
                                        <button style="margin-left: 20px" class="btn btn-success" >
                                        <?php
                                            echo $_SESSION['message'];
                                            unset($_SESSION['message']);
                                        }
                                        ?></button>
                                </h2>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Department:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="dept_id" id="dept_id" class="form-control">
                                            <option value="">Select Department</option>
                                            <?php
                                            foreach ($value as $item) {
                                                ?>
                                                <option value="<?php echo $item['id'];?>"><?php echo $item['title'];?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" id="course_id" name="course_id" class="form-control">
                                            <option value="">Select Course</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Room No:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="room_id" id="room_id" class="form-control">
                                            <option value="">Select Room</option>
                                            <?php
                                            foreach ($value1 as $item1) {
                                                ?>
                                                <option value="<?php echo $item1['id'];?>"><?php echo $item1['title'];?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Day:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="day_id" id="day_id" class="form-control">
                                            <option value="">Select Day</option>
                                            <?php
                                            foreach ($value2 as $item2) {
                                                ?>
                                                <option value="<?php echo $item2['id'];?>"><?php echo $item2['title'];?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">From:</label>
                                    <div class="col-lg-9">
                                        <input type="time" class="form-control" id="from" name="from">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">To:</label>
                                    <div class="col-lg-9">
                                        <input type="time" class="form-control" id="to" name="to">
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Assign<i class="icon-arrow-right14 position-right"></i></button>
                                </div>

                            </div>
                        </div>
                    </form>
                    <!-- /basic layout -->
                </div>

<?php
  include_once("../footer.php");
?>

<!--    		--><?php
//	} else{
//		$_SESSION['fail']= "You are not authorized!";
//		header('location:../../../index.php');
//	}

?>