<?php
include_once ("../../vendor/autoload.php");
use App\AllocateClassroom\classroom;
//echo "<pre>";
//print_r($_POST);
//die();
session_start();
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $classroom = new classroom();
    $classroom->setData($_POST);
    $checkings=$classroom->check();
    if (empty($checkings)){
        $classroom->store();
    }else{
        foreach ($checkings as $check){
            $errors=0;
            if ($check['day']==$_POST['day_id'] && $check['room']==$_POST['room_id'] && $check['from']==$_POST['day']
                && $check['to']>date("H:i:s", strtotime($_POST['from']))
                && $check['from']<date("H:i:s", strtotime($_POST['to'])))
            {
                $errors++;
            }elseif ($check['day'] == $_POST['day_id'] and $check['course'] == $_POST['course_id']) {
                if (($check['from'] <= date("H:i:s", strtotime($_POST['from']))
                        || $check['to'] > date("H:i:s", strtotime($_POST['from'])))
                    && ($check['from'] < date("H:i:s", strtotime($_POST['to']))
                        || $check['to'] >= date("H:i:s", strtotime($_POST['from'])))
                ) {
                    $errors++;
                }
            }
        }
        if ($errors==0){
            $classroom->store();
        }else{
            $_SESSION['message'] = "Overlapped";
        }
    }
}else {
    header("location:create.php");
}