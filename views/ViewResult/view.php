<?php
include_once ("../../vendor/autoload.php");
use App\Course\course;
$obj = new course();
$obj->setData($_GET);
$value = $obj->DepartmentView();
?>
<?php
//if (!empty($_SESSION['user_info'])) {
?>
<?php include_once"../header.php"; ?>


<?php include_once("../Admin/side-menubar.php"); ?>
    <style>
        td {
            border-top: 1px solid #DDDDDD;
            text-align: center;
            border-right: 1px solid #DDDDDD;
        }
        table{
            border-collapse: inherit;
        }
    </style>
    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <li><a style="color: #0a001f;" href="create.php">Add New Course</a></li>
                </ul>
            </div>
        </div>
        <!-- /page header -->

        <!-- Content area -->
        <div class="content">
            <!-- Bordered striped table -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h4 class="panel-title">Course-Assign-To-Teacher
                        <button style="margin-left: 20px" class="btn btn-success" ><?php
                            if (isset($_SESSION['message'])){
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                            } ?>
                        </button>
                    </h4>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <form method="post">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-lg-offset-3 col-lg-2 control-label" style="margin-top: 8px;font-size: 16px;font-weight: bold">Department:</label>
                            <div class="col-lg-3" style="margin-left: -80px">
                                <select data-placeholder="Select your state" name="department_id" id="department_id" class="form-control">
                                    <option value="">Select Department</option>
                                    <?php
                                    foreach ($value as $item) {
                                        ?>
                                        <option value="<?php echo $item['id'];?>"><?php echo $item['title'];?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="panel-body">
                </div>

                <div class="table-responsive">

                    <table id="view" class="table table-bordered table-striped">
                        <thead class="table-striped">
                        <tr>
                            <th style="text-align: center">Code</th>
                            <th style="text-align: center">Name</th>
                            <th style="text-align: center">Semester</th>
                            <th style="text-align: center">Assigned To</th>
                        </tr>
                        </thead>
                        <tbody id="table">

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /bordered striped table -->

        </div>

    </div>
    <!-- /main content -->

<?php
include_once("../footer.php");
?>