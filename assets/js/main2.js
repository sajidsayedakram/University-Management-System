$(document).ready(function () {
    $("#dept_id").change(function () {
        var dept_id = $(this).val();
        $.ajax({
            url: 'getTeacher.php',
            type: 'post',
            data: {depart: dept_id},
            dataType: 'json',
            success: function (response) {
                var clen = response.length;

                $("#course_id").empty();

                $("#course_id").append("<option value=''>" + 'Select Course' + "</option>");
                for (var i = 0; i < clen; i++) {
                    var id = response[i]['id'];
                    var title = response[i]['title'];
                    $("#course_id").append("<option value='" + id + "'>" + title + "</option>");
                }
            }
        });
    });
});