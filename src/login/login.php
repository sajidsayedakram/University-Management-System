<?php

namespace App\login;
use PDO;
class login
{
    private $id = "";
    private $user_role = "";
    private $user_name = "";
    private $username = "";
    private $password = "";
    private $email = "";
    private $token = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('user_name', $data)) {
            $this->user_name = $data['user_name'];
        }
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('user_role', $data)) {
            $this->user_role = $data['user_role'];
        }
        return $this;
    }//End of setData

    public function store($username, $password, $email)
    {
        try {
            $token = sha1($this->$username);
            $query = "INSERT INTO `users` (`id`, `unique_id`, `username`, `email`, `password`, `token`, `is_active`, `user_role`)
                      VALUES (:id,:uid,:username,:email,:password,:token,:active,:userrole)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => uniqid(),
                    ':username' => $username,
                    ':password' => $password,
                    ':email' => $email,
                    ':token' => $token,
                    ':active' => 1,
                    ':userrole' => 1

                )
            );

            if ($stmnt) {
                $lastId = $this->pdo->lastInsertId();
                $query = "INSERT INTO abouts (`user_id`) VALUES ($lastId)";
                $stmnt2 = $this->pdo->prepare($query);
                $stmnt2->execute(array($lastId));
                $query2 = "INSERT INTO settings (`user_id`) VALUES ($lastId)";
                $stmnt3 = $this->pdo->prepare($query2);
                $stmnt3->execute(array($lastId));
                $_SESSION['message'] = "Successfully Registered.";
                header('location:registration.php');
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }


    }//End of store

    public function login()
    {
        try {
            $query = "SELECT * FROM `users` WHERE `username` = '$this->user_name' AND `password` = '$this->password'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            if (empty($value)) {
                $_SESSION['fail'] = "Opps! Invalid email or password";
                header('location:index.php');
            } else {
                $_SESSION['user_info'] = $value;
                header('location:views/Admin/dashboard.php');
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }//End of login

    public function checkusername($username)
    {
        try {
            $query = "SELECT username FROM `users` WHERE `username` = '$username'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            if ($username == "") {
                echo "<i class=\"icon-cancel-circle2 position-left\"></i> Please Enter Username";
            } elseif ($value) {
                echo "<i class=\"icon-cancel-circle2 position-left\"></i> $username is already taken";
            } else {
                echo "<i class=\"icon-checkmark position-left\"></i> $username is Available";
            }
            return $value;
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }//End of checkusername

    public function validusername()
    {
        try {
            $username = $this->user_name;
            $password = $this->password;
            $email = $this->email;
            $query = "SELECT * FROM `users` WHERE `username` = '$username'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            if ($value) {
                $_SESSION['message'] = "Username ($username) is already taken";
                header('location:registration.php');
            } else {
                $this->store($username, $password, $email);
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }//End of login
    public function adminstore()
    {
        try {
            $query = "INSERT INTO `users` (`id`,`username`, `email`, `password`, `user_role`)
                      VALUES (:id,:username,:email,:password,:userrole)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':username' => $this->user_name,
                    ':email' => $this->email,
                    ':password' => $this->password,
                    ':userrole' => 2

                )
            );

            if ($stmnt) {
                $_SESSION['admin-message'] = "Successfully Admin Added.";
                header('location:add_admin.php');
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }


    }//End of store
    public function adminshow(){
        try{
            $query = "SELECT * FROM `users` WHERE `user_role` = 2";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of hobbiesshow
    public function adminupdateshow(){
        try{
            $query = "SELECT * FROM `users` WHERE `id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of hobbiesshow
    public function update()
    {
        try {
            $query = 'UPDATE users SET `id`=:id,`username`=:uname,`email`=:email,`password`=:password WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->id,
                    ':uname' => $this->username,
                    ':email' => $this->email,
                    ':password'=> $this->password,
                )
            );
            if ($stmnt) {
                $_SESSION['update-mess'] = "Successfully Data Updated";
                header("location:update_admin.php?id=$this->id");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `users` WHERE `users`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['admin-message'] = "Successfully Data Deleted";
                header("location:../adminpanel/adminlist.php?id=$this->id");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete

}