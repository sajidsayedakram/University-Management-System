<?php

namespace App\EnrollCourse;
use PDO;
class student
{
    private $id = "";
    private $student_id = "";
    private $student_name = "";
    private $student_mail = "";
    private $student_dept = "";
    private $course = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=varsitysystem', 'root', '');

    }
    public function setData($data='')
    {
        if (array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if (array_key_exists('student_id',$data))
        {
            $this->student_id = $data['student_id'];
        }
        if (array_key_exists('student_name',$data))
        {
            $this->student_name = $data['student_name'];
        }
        if(array_key_exists('student_mail',$data)){
            $this->student_mail = $data['student_mail'];
        }
        if(array_key_exists('student_dept',$data)){
            $this->student_dept = $data['student_dept'];
        }
        if(array_key_exists('course',$data)){
            $this->course = $data['course'];
        }
        return $this;
    }
    public function StudentView()
    {
        try{
            $query = "SELECT * FROM `students` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $dept = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $dept;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function CourseView()
    {
        try{
            $query = "SELECT * FROM `courses`";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $dept = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $dept;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function store()
    {
        try {
            $query = "INSERT INTO `enroll_a_course` (`id`, `student_id`,`student_name`,`student_mail`, `student_dept`,`course`,`date`,`created_at`) 
                              VALUES (:id,:student,:name,:mail,:dept,:course,:date,:create)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':student' => $this->student_id,
                    ':name' => $this->student_name,
                    ':mail'=>$this->student_mail,
                    ':dept' => $this->student_dept,
                    ':course' => $this->course,
                    ':date' => date('Y-m-d h:m:s'),
                    ':create'=> date('Y-m-d h:m:s'),
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Stored";
                header("location:create.php");
            }

        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }
    public function index(){
        try{
            $query = "SELECT enroll_a_course.*,courses.title as title,students.student_id as student_id FROM `enroll_a_course` LEFT JOIN `courses` ON enroll_a_course.course=courses.id LEFT JOIN `students` ON enroll_a_course.student_id=students.id";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function view(){
        try{
            $query = "SELECT enroll_a_course.id as id,enroll_a_course.student_name as name,enroll_a_course.student_mail as mail,enroll_a_course.student_dept 
as dept,enroll_a_course.course as course,enroll_a_course.date as date,students.student_id as std_id 
 FROM `enroll_a_course`,`students` WHERE enroll_a_course.id="."'".$this->id."'"."
 AND enroll_a_course.student_id=students.id";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $query = "UPDATE `enroll_a_course` SET `student_id` =:student, `student_name` =:name, `student_mail` = :mail, `student_dept` =:dept, `course` = :course,
                      `date` = :date, `updated_at` = :update WHERE id=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(
                ':id'=>$this->id,
                ':student' => $this->student_id,
                ':name' => $this->student_name,
                ':mail'=>$this->student_mail,
                ':dept' => $this->student_dept,
                ':course' => $this->course,
                ':date' => date('Y-m-d'),
                ':update'=>date('Y-m-d h:m:s'),
            ));
            if ($stmt) {
                $_SESSION['update-message'] = "Successfully Data Updated";
                header("location:edit.php?id=$this->id");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $query = "DELETE FROM enroll_a_course WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../EnrollCourse/view.php");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}