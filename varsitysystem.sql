-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2017 at 08:59 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `varsitysystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(100) NOT NULL,
  `dept_id` int(100) UNSIGNED NOT NULL,
  `semester_id` int(100) UNSIGNED NOT NULL,
  `credit` int(100) NOT NULL,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `dept_id`, `semester_id`, `credit`, `code`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(43, 17, 1, 3, 'EEE-101', 'ELECTRIC', 'ELECTRIC', '2017-05-16 08:05:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 17, 3, 3, 'EEE-103', 'Digital Electronics', 'Digital Electronics', '2017-05-15 11:05:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 24, 4, 3, 'PHP', 'oop-basic', 'ddddddddddddd', '2017-05-29 05:05:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_class_mapping`
--

CREATE TABLE `course_class_mapping` (
  `id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `from` time NOT NULL,
  `to` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_class_mapping`
--

INSERT INTO `course_class_mapping` (`id`, `dept_id`, `course_id`, `room_id`, `day_id`, `from`, `to`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 8, 0, 1, 7, '10:00:00', '11:30:00', '2017-05-07 12:05:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 7, 28, 1, 7, '10:00:00', '11:30:00', '2017-05-07 12:05:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_teacher_mapping`
--

CREATE TABLE `course_teacher_mapping` (
  `id` int(100) NOT NULL,
  `dept_id` int(100) NOT NULL,
  `teacher_id` int(100) NOT NULL,
  `total_credit` int(100) NOT NULL,
  `remain_credit` int(100) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_credit_c` int(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_teacher_mapping`
--

INSERT INTO `course_teacher_mapping` (`id`, `dept_id`, `teacher_id`, `total_credit`, `remain_credit`, `course_code`, `course_name`, `course_credit_c`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 8, 8, 30, 30, '27', 'Computer Fundamental', 3, '2017-05-04 09:05:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 8, 8, 30, 30, '30', 'Web Development', 3, '2017-05-04 09:05:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 10, 9, 20, 20, '29', 'C Language', 3, '2017-05-05 07:05:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 24, 21, 20, 20, '45', 'oop-basic', 3, '2017-05-29 05:05:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Saturday', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Sunday', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Monday', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Tuesday', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Wednesday', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Thursday ', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Friday ', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Sat-Mon-Wed', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Sun-Tues-Thu', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(100) NOT NULL,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `code`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(17, 'EEE-101', 'Electrical Electronics Engineering', '2017-05-16 07:05:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'CSE-102', 'Computer Science & Engineering', '2017-05-16 07:05:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'PHP', 'Php- Hyper Text Preprocessor', '2017-05-29 05:05:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dept. Head', '2017-05-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Senior Lecturer ', '2017-05-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'lecturer', '2017-05-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `enroll_a_course`
--

CREATE TABLE `enroll_a_course` (
  `id` int(100) NOT NULL,
  `student_id` int(11) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `student_mail` varchar(255) NOT NULL,
  `student_dept` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enroll_a_course`
--

INSERT INTO `enroll_a_course` (`id`, `student_id`, `student_name`, `student_mail`, `student_dept`, `course`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(11, 35, 'Mitu', 'mitu@m.com', 'Electrical Electronics Engineering', '43', '2017-05-15', '2017-05-15 11:05:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 35, 'Mitu', 'mitu@m.com', 'Electrical Electronics Engineering', '44', '2017-05-15', '2017-05-15 11:05:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 35, 'Mitu', 'mitu@m.com', 'Electrical Electronics Engineering', '44', '2017-05-15', '2017-05-15 11:05:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'A+', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'A', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'A-', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'B+', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'B', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'B-', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'C+', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'C', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'C-', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'D+', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'D', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'D-', '2017-05-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'RM-101', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'RM-102', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'RM-103', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'RM-104', '2017-05-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE `semesters` (
  `id` int(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1st Semester', '2017-05-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '2nd Semester', '2017-05-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '3rd Semester', '2017-05-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '4th Semester', '2017-05-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(100) NOT NULL,
  `department` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `mail` text NOT NULL,
  `contact` int(100) NOT NULL,
  `date` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `department`, `title`, `mail`, `contact`, `date`, `address`, `student_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(30, '17', 'Mitu', 'mitu@m.com', 503982, '2017-05-10', 'dddddddd', '-2017-001', '2017-05-16 05:05:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '23', 'fgh', 'g@c.com', 2334, '2017-05-04', 'gggd', '-2017-001', '2017-05-16 05:05:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '23', 'fgh', 'g@c.com', 2334, '2017-05-04', 'gggd', 'S-2017-002', '2017-05-16 05:05:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '17', 'fgh', 'g@c.com', 2334, '2017-05-04', 'gggd', 'S-2017-002', '2017-05-16 05:05:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '17', 'Mitu', 'mitu@m.com', 503982, '2017-05-10', 'dddddddd', '-2017-003', '2017-05-16 05:05:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'EEE-101', 'Mitu', 'mitu@m.com', 177846812, '2017-05-11', 'ddddddd', 'EEE-2017-001', '2017-05-16 08:05:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_results`
--

CREATE TABLE `student_results` (
  `id` int(100) NOT NULL,
  `student_id` int(100) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `student_mail` varchar(255) NOT NULL,
  `student_dept` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `grade_id` int(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(100) NOT NULL,
  `dept_id` int(100) NOT NULL,
  `designation_id` int(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `contact` int(100) NOT NULL,
  `credit` int(100) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `deleted_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `dept_id`, `designation_id`, `title`, `address`, `mail`, `contact`, `credit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(11, 17, 1, 'dddddd', 'dddddddd', 'ddddddd@b.com', 32, 3, 2017, 0, 0),
(12, 23, 3, 'dddddd', 'aaaaa', 'tiemoon@gmail.com', 22222, 2, 2017, 0, 0),
(17, 17, 1, 'dddddd', 'ssss', 'ddddddd1111@b.com', 2147483647, 1, 2017, 0, 0),
(20, 17, 1, 'dddddd', 'eeeeeee', 'ddddddd33333@b.com', 3333333, 3, 2017, 0, 0),
(21, 24, 2, 'Sumon', 'dddd', 'sumon@gmail.com', 1132897328, 20, 2017, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_class_mapping`
--
ALTER TABLE `course_class_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dept_id` (`dept_id`,`course_id`,`day_id`);

--
-- Indexes for table `course_teacher_mapping`
--
ALTER TABLE `course_teacher_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `course_code_2` (`course_code`),
  ADD KEY `course_code` (`course_code`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`,`title`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enroll_a_course`
--
ALTER TABLE `enroll_a_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_results`
--
ALTER TABLE `student_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `course_class_mapping`
--
ALTER TABLE `course_class_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `course_teacher_mapping`
--
ALTER TABLE `course_teacher_mapping`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `enroll_a_course`
--
ALTER TABLE `enroll_a_course`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `student_results`
--
ALTER TABLE `student_results`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
